\documentclass{article}

\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}

\author{
  Samuel Grush\\
  \and
  Shawn McCormick
}
\title{CS6601 Homework 2}

\begin{document}

\maketitle

\newcommand\tab{\hspace*{0.9cm}}
\newcommand\rhat{\hat{r}}
\newcommand\ZN{\ensuremath{\mathbb{Z}_N}}

\section{Problem}
This paper describes a two-stage two-party secure minimum protocol. The first stage compares u and v. The second stage computes the minimum of the two values.

\section*{Stage 1}
Input $P_1$: E(u), E(v), pk \\
Input $P_2$: sk

\begin{enumerate}
  \item \textbf{$P_1$}\\
    $E(u)[] \leftarrow \text{SBD}(E(u))$ \\
    $E(v)[] \leftarrow \text{SBD}(E(v))$ \\
    $W, G, H, L, \Phi, r \leftarrow [~]$ \\
    $l \leftarrow \text{len}(v)$ \\
    $F \leftarrow~\in_R \{\texttt{"u > v"}, \texttt{"u < v"}\}$ \\
    \textbf{for} $i = 1$ to $l$ \textbf{do}: \\
      \tab  $E(u_i \times v_i) \leftarrow \text{SM}(E(u)_i, E(v)_i)$ \\
      \tab  \textbf{if} $F \equiv \texttt{"u > v"}$ \textbf{then}: \\
      \tab  \tab  $W_i \leftarrow E(u_i) \times E(u_i \times v_i)^{N-1}$ \\
      \tab  \textbf{else}: \\
      \tab  \tab  $W_i \leftarrow E(v_i) \times E(u_i \times v_i)^{N-1}$ \\
      \tab  $G_i \leftarrow E(u_i) \times E(v_i) \times E(u_i \times v_i)^{N-2}$ \\
      \tab  $r_i \leftarrow~\in_R \ZN$ \\
      \tab  $H_i \leftarrow H_{i-1}^{r_i} \times G_i \mid H_0 = E(0)$ \\
      \tab  $\Phi_i \leftarrow E(-1) \times H_i$ \\
      \tab  $r'_i \leftarrow~\in_R \ZN$ \\
      \tab  $L_i \leftarrow W_i \times \Phi_i^{r'_i}$ \\
    $L' \leftarrow \pi_1(L)$ \\
    \textbf{send $L'$ to $P_2$}

  \item \textbf{$P_2$} \\
    \textbf{receive $L'$ from $P_1$} \\
    $M \leftarrow [~]$ \\
    \textbf{for} $i = 1$ to $l$ \textbf{do}: \\
      \tab  $M_i \leftarrow D(L'_i)$ \\
    \textbf{if} ($\exists j \mid M_j \equiv 1$) \textbf{then}: \\
      \tab  $\alpha \leftarrow 1$ \\
    \textbf{else}: \\
      \tab  $\alpha \leftarrow 0$ \\
    \textbf{send $E(\alpha)$ to $P_1$}

  \item \textbf{$P_1$} \\
    \textbf{receive $E(\alpha)$ from $P_2$} \\

\end{enumerate}

Choosing the functionality at random prevents information from being leaked to $P_2$ about which number is greater.
Although $P_2$ will find out whether the left or right operand is greater, 
it does not know which operand corresponds to u or v. 

First, inputs u and v are decomposed into encrypted bit array representations using a secure bit decomposition protocol. 
W is calculated as $u_i-u_iv_i$ (or $v_i-u_iv_i$ for $F \equiv v>u$) 
which results in 1, IFF $u_i > v_i$ (or $v_i > u_i$ for $F \equiv v>u$), else 0.
Since only the first bit difference matters in a comparison (starting at the MSB), 
the next steps randomize all of the values except the index of the first difference, which is (1 if F(u,v) else r). 

$P_1$ cannot perform the comparison since it does not have the secret key, 
so the L array is permuted and passed to $P_2$.
The randomization of non-1 bits and permutation of their order prevents 
information from being leaked about the magnitude of the variables.

$P_2$ decrypts L to $M=[D(L_1), D(L_2), ... D(L_l)]$.
$\alpha$ is 1 if 1$\in$ M, else 0.
$P_2$ returns E($\alpha$) to $P_1$ which represents the encrypted result of the comparison F(u,v)

\section*{Stage 2}
The following inputs are carried over from stage 1. 
$\alpha$ could either be stored by $P_2$ from stage 1, 
or sent as E($\alpha$) by $P_1$ and decrypted using sk if a new session is used. \\
Input $P_1$: E(u)[], E(v)[], $\Gamma$, $\rhat$, pk \\
Input $P_2$: $\alpha$, sk
\begin{enumerate}
  \item \textbf{$P_1$} \\
    $\Gamma, \rhat \leftarrow [~]$ \\
    \textbf{for} $i = 1$ to $l$ \textbf{do}: \\
      \tab  $\rhat_i \leftarrow~\in_R \ZN$ \\
      \tab  \textbf{if} $F \equiv \texttt{"u > v"}$ \textbf{then}: \\
      \tab  \tab  $\Gamma_i \leftarrow E(v_i) \times E(u_i)^{N-1} \times E(\rhat_i)$ \\
      \tab  \textbf{else}: \\
      \tab  \tab  $\Gamma_i \leftarrow E(u_i) \times E(v_i)^{N-1} \times E(\rhat_i)$ \\
    $\Gamma' \leftarrow \pi_2(\Gamma)$ \\
    \textbf{send $\Gamma$ to $P_2$}

  \item \textbf{$P_2$} \\
    \textbf{receive $\Gamma$ from $P_1$} \\
    $M' \leftarrow [~]$ \\
    \textbf{for} $i = 1$ to $l$ \textbf{do}: \\
      \tab  $M'_i \leftarrow {\Gamma'_i}^\alpha$ \\
    \textbf{send $M'$ to $P_1$}

  \item \textbf{$P_1$} \\
    \textbf{receive $M'$ from $P_2$} \\
    $\lambda, E(\text{min}(u,v)) \leftarrow [~]$ \\
    $\widetilde{M} \leftarrow \pi_2^{-1}(M')$ \\
    \textbf{for} $i = 1$ to $l$ \textbf{do}: \\
      \tab  $\lambda_i \leftarrow \widetilde{M}_i \times E(\alpha)^{N-\rhat_i}$ \\
      \tab  \textbf{if} $F \equiv \texttt{"u > v"}$ \textbf{then}: \\
      \tab  \tab  $E(\text{min}(u,v)_i) \leftarrow E(u_i) \times \lambda_i$ \\
      \tab  \textbf{else}: \\
      \tab  \tab  $E(\text{min}(u,v)_i) \leftarrow E(v_i) \times \lambda_i$ \\

$P_1$ computes $\Gamma$, which is the differences between each encrypted bit of u,v. Since $\alpha$ was encrypted to prevent $P_1$ from knowing which of the values is greater, raising to the power $\alpha$ must be computed by $P_2$.

$\Gamma$ is permuted and sent to $P_2$ where its bits are raised to the power $\alpha$.
This result is sent as $M'$ to $P_1$, the permutation operation is inversed, and the randomization is removed, resulting in $\lambda$.

For $F \equiv "u > v"$, $\lambda$ = $E(v-u)$ if $u>v$ else E(0), so\\
E(u) * $\lambda$ = $E(u)*E(0)=E(u)$, \tab $u>v$ \\
\tab \tab $E(u)*E(v-u) = E(v)$, \tab otherwise

For $F \equiv "v > u"$, $\lambda$ = $E(u-v)$ if $v>u$ else E(0), so\\
E(v) * $\lambda$ = $E(v)*E(0)=E(v)$, \tab $v>u$ \\
\tab \tab $E(v)*E(u-v) = E(u)$, \tab otherwise

Thus, E(min(u,v)) is calculated as desired.

\end{enumerate}

\section{Example}
\begin{enumerate}
\item $P_i$ (Stage 1.1) \\ 
Let [u] = 101, [v] = 110 \\
$F \equiv "u < v"$ \\
$W \leftarrow$  E[1 - 1*1, 1-0*1, 0-1*0] = [E(0), E(1), E(0)] \\
$G \leftarrow$ [E(0), E(1), E(1)] \\
$H \leftarrow$ [E(0), E(1), E(r)] \\
$\Phi \leftarrow$ [E(N-1),E(1),E(r)] \\
$L \leftarrow$ [E(r), E(1), E(r)] \\
$L' \leftarrow \pi_1(L)$ = [E(1), E(r), E(r)]

\item $P_2$ (Stage 1.2) \\
receive $L'$ \\
$M \leftarrow D(L') = [1, r, r]$ \\
$\alpha \leftarrow 1$ since $1 \in M$

\item $P_1$ (Stage 1.3) \\
receive E($\alpha$)

\item $P_1$ (Stage 2.1) \\
$\Gamma \leftarrow [E(1-1+r), E(0-1+r), E(1-0+r)] = [E(0+r), E(-1+r), E(1+r)]$\\
$\Gamma' \leftarrow \pi_2(\Gamma) = [E(1+r), E(-1+r), E(r)]$\\

\item $P_2$ (Stage 2.2) \\
receive $\Gamma'$, $\alpha$ \\
$\alpha \leftarrow D_{sk}(E_{pk}(\alpha))$ \\
$M' \leftarrow [\Gamma_1^{'\alpha}, ...] = [E(1+r), E(-1+r), E(r)]$

\item $P_1$ (Stage 2.3) \\
receive $M'$ \\
$\widetilde{M} \leftarrow \pi_2^{-1}(M') = [E(r), E(-1+r), E(1+r)]$\\
$\lambda \leftarrow [E(0), E(-1), E(1)]$ \\
E(min(u,v)) $\leftarrow E(v)*\lambda = [E(1)*E(0), E(1)*E(-1), E(0)*E(1)]$ \\
\tab $=[E(1), E(0), E(1)]$


\end{enumerate}

\end{document}
